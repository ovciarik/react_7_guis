import React from 'react'
import { useState } from 'react'

enum FlightType {
  OneWay = "one-way flight",
  Return = "return flight",
}

export default function FlightBooker() {
  const [flightType, setFlightType] = useState(FlightType.OneWay)
  const [dateA, setDateA] = useState(new Date('2000-01-01'))
  const [dateB, setDateB] = useState(new Date('2000-01-01'))

  function isBookDisabled() {
    if (flightType === FlightType.OneWay) {
      return false
    } else {
      return !(dateA.valueOf() <= dateB.valueOf())
    }
  }

  function setFlightTypeEnum(flightType: string) {
    if (flightType === FlightType.OneWay) {
      setFlightType(FlightType.OneWay)
    } else {
      setFlightType(FlightType.Return)
    }
  }

  return (
    <div
      className='border border-gray-700 rounded p-4 flex flex-col space-y-4 text-2xl'
    >
      <select className='bg-gray-600' value={flightType} onChange={(e) => setFlightTypeEnum(e.target.value)}>
        {Object.values(FlightType).map(ftype => <option key={ftype}>{ftype}</option>)}
      </select>
      <input
        className='bg-gray-600'
        onChange={(e) => setDateA(new Date(e.target.value))}
        type='date'
        value={dateA.toISOString().substring(0, 10)}
      ></input>
      <input
        className='bg-gray-600 disabled:text-gray-700'
        disabled={flightType === FlightType.OneWay}
        onChange={(e) => setDateB(new Date(e.target.value))}
        type='date'
        value={dateB.toISOString().substring(0, 10)}></input>
      <button
        className='border border-blue-700 rounded p-2 disabled:text-gray-700 disabled:border-gray-700'
        disabled={isBookDisabled()}
      >Book</button>
    </ div>
  );
}