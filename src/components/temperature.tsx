import React from 'react'
import { useState } from 'react'

export default function Temperature() {
  const [counter, setCounter] = useState(0)

  return (
    <div className="border border-gray-700 py-4 px-8 rounded space-x-8 flex items-center">
      <span>
        <input
          name='celsius'
          type='number'
          className='bg-gray-800 text-gray-200 border boreder-gray-300 rounded'
          value={Math.round(counter)}
          onChange={(e) => { setCounter(Number(e.target.value)) }}
        ></input>
        <label htmlFor='celsius'>Celsius</label>
      </span>
      <span>=</span>
      <span>
        <input
          name='f'
          type='number'
          className='bg-gray-800 text-gray-200 border boreder-gray-300 rounded'
          value={Math.round(counter * (9 / 5) + 32)}
          onChange={(e) => { setCounter((Number(e.target.value) - 32) * 5 / 9) }}
        ></input>
        <label htmlFor='f'>Fahrenheit</label>
      </span>
    </div>
  );
}