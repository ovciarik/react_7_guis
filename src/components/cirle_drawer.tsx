import React from 'react'
import { useRef, useState, useEffect } from 'react'

const BACKGROUD_COLOR = '#CCCCCC'
const HIGHLIGHT_COLOR = '#444444'
const STROKE_COLOR = '#000000'

const DEFAULT_RADIUS = 10

type Circle = {
  x: number,
  y: number,
  screen_x: number,
  screen_y: number,
  radius: number,
  path2d: Path2D,
}

type Action = {
  type: 'create',
  idx: number,
  elem: Circle,
} | {
  type: 'update',
  idx: number,
  oldRadius: number,
}


function createCircle({ x, y, screen_x, screen_y, radius }: { x: number, y: number, screen_x: number, screen_y: number, radius: number }): Circle {
  const tmpCircle = new Path2D();
  tmpCircle.arc(x, y, radius, 0, 2 * Math.PI, false);
  return {
    x,
    y,
    screen_x,
    screen_y,
    radius,
    path2d: tmpCircle,
  }

}

function ContextMenu({ hlCircle, setHlCircle, setContext, saveHlCircle }: any) {
  return <div className='top-0 left-0 w-screen h-screen z-20 fixed overflow-none' onClick={() => { setContext(false); saveHlCircle() }}>
    <div className='flex absolute' style={{ top: hlCircle.screen_y, left: hlCircle.screen_x }} >
      <div className='bg-gray-900 p-2 border border-gray-700 rounded' onClick={(e) => e.stopPropagation()} >
        <div>Adjust diameter of a circle at ({hlCircle.x},{hlCircle.y})</div>
        <input type='range' value={hlCircle.radius} onChange={(e) => setHlCircle((current: any) => { return { ...current, radius: Number(e.target.value) } })}></input>
      </div>
    </div>
  </div >
}

export default function CircleDrawer() {
  const canvasRef = useRef(null)
  const [circleBuffer, setCircleBuffer] = useState<Circle[]>([])
  const [undoBuffer, setUndoBuffer] = useState<Action[]>([])
  const [redoBuffer, setRedoBuffer] = useState<Action[]>([])
  const [hlCircle, setHlCircle] = useState<Circle | null>(null)
  const [context, setContext] = useState(false)

  useEffect(() => {
    const canvas = canvasRef.current
    const ctx = canvas.getContext('2d')
    ctx.fillStyle = BACKGROUD_COLOR
    ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height)
    ctx.strokeStyle = STROKE_COLOR;
    ctx.fillStyle = HIGHLIGHT_COLOR;

    circleBuffer.forEach((circle) => {
      if (hlCircle !== null && circle.path2d === hlCircle.path2d) {
        const tmpCircle = new Path2D();
        tmpCircle.arc(circle.x, circle.y, hlCircle.radius, 0, 2 * Math.PI, false);
        ctx.fill(tmpCircle);
        ctx.stroke(tmpCircle);
      } else {
        ctx.stroke(circle.path2d);
      }
    })

  }, [circleBuffer, hlCircle])


  function saveHlCircle() {
    let index = 0
    circleBuffer.forEach((circle, idx) => {
      if (hlCircle !== null && circle.path2d === hlCircle.path2d) {
        index = idx
      }
    })
    const selCircle = circleBuffer[index]

    const circleObj = createCircle({ ...selCircle, radius: hlCircle.radius })
    setHlCircle(circleObj)
    setCircleBuffer((circleBuffer) => [...circleBuffer.slice(0, index), circleObj, ...circleBuffer.slice(index + 1)])
    setUndoBuffer((undoBuffer) => [...undoBuffer, { type: 'update', idx: index, oldRadius: selCircle.radius }])
    setRedoBuffer([])
  }

  function handleClick(e: any) {
    let clickInside = false
    const x = e.clientX + window.scrollX - e.target.offsetLeft
    const y = e.clientY + window.scrollY - e.target.offsetTop
    const screen_x = e.clientX
    const screen_y = e.clientY
    console.log(e)
    console.log(window.document.body.scrollTop)
    console.log(window.document)
    console.log(window.scrollX)
    console.log(window.scrollY)

    const canvas = canvasRef.current
    const ctx = canvas.getContext('2d')
    circleBuffer.forEach((circle, idx) => {
      const tmp = ctx.isPointInPath(circle.path2d, x, y)
      if (tmp) {
        clickInside = true
      }
    })

    if (!clickInside) {
      const circleObj = createCircle({ x, y, screen_x, screen_y, radius: DEFAULT_RADIUS })
      setHlCircle(circleObj)
      setCircleBuffer((circleBuffer) => [...circleBuffer, circleObj])
      setUndoBuffer((undoBuffer) => [...undoBuffer, { type: 'create', idx: circleBuffer.length, elem: circleObj }])
      setRedoBuffer([])
    } else {
      setContext(true)
    }
  }

  function handleMouseMove(e: any) {
    setHlCircle(null)
    const x = e.clientX + window.scrollX - e.target.offsetLeft
    const y = e.clientY + window.scrollY - e.target.offsetTop

    const canvas = canvasRef.current
    const ctx = canvas.getContext('2d')
    circleBuffer.forEach((circle) => {
      const tmp = ctx.isPointInPath(circle.path2d, x, y)
      if (tmp) {
        setHlCircle(circle)
      }
    })
  }

  function handleUndo() {
    const action = undoBuffer.slice(-1)[0]
    if (action.type === 'create') {
      setUndoBuffer((undoBuffer) => [...undoBuffer.slice(0, -1)])
      setCircleBuffer((circleBuffer) => [...circleBuffer.slice(0, action.idx), ...circleBuffer.slice(action.idx + 1)])
      setRedoBuffer((redoBuffer) => [...redoBuffer, { type: 'create', elem: circleBuffer[action.idx], idx: action.idx }])
    } else {
      setUndoBuffer((undoBuffer) => [...undoBuffer.slice(0, -1)])
      setCircleBuffer((circleBuffer) => [...circleBuffer.slice(0, action.idx), createCircle({ ...circleBuffer[action.idx], radius: action.oldRadius }), ...circleBuffer.slice(action.idx + 1)])
      setRedoBuffer((redoBuffer) => [...redoBuffer, { type: 'update', idx: action.idx, oldRadius: circleBuffer[action.idx].radius }])
    }
  }

  function handleRedo() {
    const action = redoBuffer.slice(-1)[0]
    if (action.type === 'create') {
      setRedoBuffer((redoBuffer) => [...redoBuffer.slice(0, -1)])
      setCircleBuffer((circleBuffer) => [...circleBuffer.slice(0, action.idx), action.elem, ...circleBuffer.slice(action.idx + 1)])
      setUndoBuffer((undoBuffer) => [...undoBuffer, { type: 'create', idx: action.idx, elem: action.elem }])
    } else {
      setRedoBuffer((redoBuffer) => [...redoBuffer.slice(0, -1)])
      setCircleBuffer((circleBuffer) => [...circleBuffer.slice(0, action.idx), createCircle({ ...circleBuffer[action.idx], radius: action.oldRadius }), ...circleBuffer.slice(action.idx + 1)])
      setUndoBuffer((undoBuffer) => [...undoBuffer, { type: 'update', idx: action.idx, oldRadius: circleBuffer[action.idx].radius }])
    }
  }

  return (
    <div className="border border-gray-700 py-4 px-8 rounded space-y-2">
      <div className="flex space-x-2">
        <input
          type='button'
          value='Undo'
          className='border border-blue-500 rounded bg-blue-500 px-1 disabled:border-gray-700 disabled:bg-gray-700'
          onClick={() => handleUndo()}
          disabled={undoBuffer.length === 0}
        ></input>
        <input
          type='button'
          value='Redo'
          className='border border-blue-500 rounded bg-blue-500 px-1 disabled:border-gray-700 disabled:bg-gray-700'
          onClick={() => handleRedo()}
          disabled={redoBuffer.length === 0}
        ></input>
      </div>
      <canvas ref={canvasRef} width="500" height="300" id="canvas" onClick={handleClick} onMouseMove={handleMouseMove}></canvas>
      {context === true && <ContextMenu hlCircle={hlCircle} setHlCircle={setHlCircle} setContext={setContext} saveHlCircle={saveHlCircle} />}
    </div >
  );
}