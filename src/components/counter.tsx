import React from 'react'
import { useState } from 'react'

export default function Counter() {
  const [counter, setCounter] = useState(0)

  return (
    <div className="border border-gray-700 py-2 px-8 rounded space-x-8 flex items-center">
      <div>
        {counter}
      </div>
      <button
        onClick={() => setCounter(counter + 1)}
        className="border border-blue-700 p-2 m-2 rounded hover:bg-blue-700"
      >add</button>
    </div>
  );
}