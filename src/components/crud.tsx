import React from 'react'
import { useState } from 'react'

export default function Crud() {
  const [list, setList] = useState([
    {
      name: 'Hans',
      surname: 'Emil',
    },
    {
      name: 'Max',
      surname: 'Mustermann',
    },
    {
      name: 'Roman',
      surname: 'Tischer',
    },
  ])

  const [name, setName] = useState('')
  const [surname, setSurname] = useState('')
  const [selectedIndex, setSelectedIndex] = useState<number | void>(null)
  const [filterState, setFilterState] = useState('')

  const filteredList = list
    .map((elem, idx) => { return { ...elem, idx } })
    .filter(elem => `${elem.name}, ${elem.surname}`.includes(filterState))

  function handleSelectChange(e: React.ChangeEvent<HTMLSelectElement>) {
    const index = filteredList[e.target.selectedIndex].idx
    setSelectedIndex(index)
    setName(list[index].name)
    setSurname(list[index].surname)
  }

  function handleCreate() {
    setList([...list, { name, surname }])
  }

  function handleUpdate() {
    if (typeof (selectedIndex) === 'number') {
      const actuallIndex = filteredList[selectedIndex].idx
      const newEntry = { name, surname }
      setList([...list.slice(0, actuallIndex), newEntry, ...list.slice(actuallIndex + 1)])
    }
  }

  function handleDelete() {
    if (typeof (selectedIndex) === 'number') {
      const actuallIndex = filteredList[selectedIndex].idx
      setList([...list.slice(0, actuallIndex), ...list.slice(actuallIndex + 1)])
    }
  }

  return (
    <div className="border border-gray-700 py-4 px-8 rounded space-y-2">
      <div>
        <label htmlFor='filterPrefix'>Filter prefix:</label>
        <input
          value={filterState}
          onChange={(e) => setFilterState(e.target.value)}
          type='text' name='filterPrefix' style={{ width: '5ch' }} className='bg-gray-700' />
      </div>
      <div className='flex'>
        <select size={5} className='bg-gray-700' onChange={handleSelectChange}>
          {filteredList.map((elem, idx) =>
            <option key={idx}>{`${elem.name}, ${elem.surname}`}</option>)}
        </select>
        <div className='space-y-4 ml-2'>
          <div className='flex justify-between space-x-4'>
            <label>Name:</label>
            <input
              className='bg-gray-700'
              onChange={(e) => setName(e.target.value)}
              value={name}
            ></input>
          </div>
          <div className='flex justify-between space-x-4'>
            <label>Surname:</label>
            <input
              className='bg-gray-700'
              onChange={(e) => setSurname(e.target.value)}
              value={surname}
            ></input>
          </div>
        </div>
      </div>
      <div className='flex space-x-2'>
        <button
          className='border border-blue-700 rounded p-2 hover:bg-blue-700'
          onClick={handleCreate}
        >Create</button>
        <button
          className='border border-blue-700 rounded p-2 hover:bg-blue-700'
          onClick={handleUpdate}
        >Update</button>
        <button
          className='border border-blue-700 rounded p-2 hover:bg-blue-700'
          onClick={handleDelete}
        >Delete</button>
      </div>
    </div >
  )
}