import React from 'react'
import { useState, useEffect } from 'react'

export default function Timer() {
  const [isActive, setIsActive] = useState(true)
  const [time, setTime] = useState(0)
  const [duration, setDuration] = useState(50)

  useEffect(() => {
    let interval: any = null;
    if (isActive) {
      interval = setInterval(() => {
        setTime((seconds) => {
          if (seconds + 2 > duration) {
            setIsActive(false)
          }
          return seconds + 1;
        });
      }, 100);
    }
    return () => clearInterval(interval);
  }, [isActive, duration]);

  return (
    <div className="border border-gray-700 py-4 px-8 rounded space-y-2">
      <div>
        <label htmlFor="ellapsedTime">Elapsed Time:</label>
        <progress id="ellapsedTime" max={duration} value={time}></progress>
      </div>
      <div>{(time / 10).toFixed(1)}</div>
      <div>
        <label htmlFor="duration">Duration:</label>
        <input
          onChange={(e) => {
            setDuration(Number(e.target.value));
            // eslint-disable-next-line @typescript-eslint/no-unused-expressions
            (Number(e.target.value) < time) ? setTime(Number(e.target.value)) : ''
            setIsActive(true)
          }}
          type="range" id="duration" name="duration" min="0" max="200" value={duration} step="1" />
      </div>
      <button
        onClick={() => { setTime(0); setIsActive(true) }}
        className='border border-blue-700 p-2 hover:bg-blue-700 rounded'
      >reset</button>
    </div >
  );
}