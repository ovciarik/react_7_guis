import React from 'react'
import { useState, useEffect } from 'react'

const SUM_REGEX = /=sum\(([A-Z])(\d+):([A-Z])(\d+)\)/
const A_OFFSET = 'A'.charCodeAt(0)

function evalCell(table: readonly string[][], cell: string, r_idx: number, c_idx: number, path: readonly string[]): string {
  const currentId = `${r_idx}, ${c_idx}`
  if (path.includes(currentId)) {
    throw Error('#ERR_LOOP')
  } else if (cell === '') {
    return ''
  } else if (SUM_REGEX.test(cell)) {
    const match = cell.match(SUM_REGEX)
    const [_, s_start_col, s_start_row, s_stop_col, s_stop_row] = match
    const start_col = s_start_col.charCodeAt(0) - A_OFFSET
    const start_row = Number(s_start_row)
    const stop_col = s_stop_col.charCodeAt(0) - A_OFFSET
    const stop_row = Number(s_stop_row)
    const slice = table
      .slice(start_row, stop_row + 1)
      .map((row) => row.slice(start_col, stop_col + 1))
    try {
      const sum: number = slice
        .map((row, row_index) => row
          .map(
            (cell, cell_index) =>
              Number(evalCell(
                table,
                cell,
                start_row + row_index,
                start_col + cell_index,
                [...path, currentId]
              ))
          )
          .reduce((a, b) => a + b, 0))
        .reduce((a, b) => a + b, 0)
      return sum.toString()
    } catch (err) {
      return err.message
    }
  }
  return String(Number(cell))
}

function evaluate(rawTable: readonly string[][]): string[][] {
  let newTable: string[][] = []
  rawTable.forEach((row, r_idx) => {
    newTable.push([...row.map((cell, c_idx) => evalCell(rawTable, cell, r_idx, c_idx, []))])
  })
  return newTable
}

export default function Cells() {

  const [rawTable, setRawTable] = useState(Array(99).fill(Array).map(() => Array(26).fill('')))
  const [displayTable, setDisplayTable] = useState(Array(99).fill(Array).map(() => Array(26).fill('')))
  const [selected, setSelected] = useState([-1, -1])

  useEffect(() => {
    setDisplayTable(evaluate(rawTable))
  }, [rawTable])

  function changeCell(r: number, c: number, value: string) {
    let newTable: string[][] = []
    rawTable.forEach((row) => {
      newTable.push([...row])
    })
    newTable[r][c] = value
    setRawTable(newTable)
  }

  return (
    <div className="border border-gray-700 py-2 px-8 rounded space-x-8 flex items-center w-4/5 h-96 overflow-auto">
      <div className='h-full w-full overflow-scroll'>
        <table>

          <thead>
            <tr>
              <th></th>
              {displayTable[0].map((col, c) => {
                return <th key={c}>{String.fromCharCode(c + A_OFFSET)}</th>
              })}
            </tr>
          </thead>
          <tbody >
            {displayTable.map(
              (xrow, r) => <tr key={r}><th>{r}</th>{xrow.map((xcell, c) =>
                <td className='p-0 m-0' key={`${r},${c}`}>
                  {[r, c].toString() !== selected.toString() ?
                    <input
                      className='bg-gray-800 border border-gray-700 p-0 m-0 w-24'
                      value={xcell}
                      onFocus={(() => setSelected([r, c]))}
                      onChange={() => { }}
                    /> :
                    <input
                      className='bg-gray-800 border border-gray-700 p-0 m-0 w-24'
                      value={rawTable[r][c]}
                      onChange={(e) => changeCell(r, c, e.target.value)}
                      onBlur={() => setSelected([-1, -1])}
                      onKeyDown={(e: any) => { e.key === 'Enter' && e.target.blur() }}
                    />
                  }
                </td>
              )}</tr>)}
          </tbody>
        </table>
      </div>
    </div>
  );
}