import React from 'react'
import Counter from './components/counter'
import Temperature from './components/temperature'
import FlightBooker from './components/flight_booker'
import Timer from './components/timer'
import Crud from './components/crud'
import CircleDrawer from './components/cirle_drawer'
import Cells from './components/cells'

function App() {
  return (
    <div className="bg-gray-800 text-gray-200 min-h-screen flex flex-col space-y-2 p-2">
      <div className='flex'>
        <Counter />
      </div>
      <div className='flex'>
        <Temperature />
      </div>
      <div className='flex'>
        <FlightBooker />
      </div>
      <div className='flex'>
        <Timer />
      </div>
      <div className='flex'>
        <Crud />
      </div>
      <div className='flex'>
        <CircleDrawer />
      </div>
      <div className='flex'>
        <Cells />
      </div>
    </div>
  );
}

export default App;
